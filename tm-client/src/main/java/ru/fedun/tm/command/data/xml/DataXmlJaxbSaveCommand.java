package ru.fedun.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.SessionDTO;

public final class DataXmlJaxbSaveCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-save-xml-jb";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to xml (jax-b) file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML (JAX-B) SAVE]");
        @NotNull final SessionDTO session = serviceLocator.getSessionService().getCurrentSession();
        serviceLocator.getDataEndpoint().saveXmlByJaxb(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
