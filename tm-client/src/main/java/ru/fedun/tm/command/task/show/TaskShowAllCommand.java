package ru.fedun.tm.command.task.show;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.Session;
import ru.fedun.tm.endpoint.SessionDTO;
import ru.fedun.tm.endpoint.Task;
import ru.fedun.tm.endpoint.TaskDTO;

import java.util.List;

public final class TaskShowAllCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LIST TASKS]");
        @NotNull final SessionDTO session = serviceLocator.getSessionService().getCurrentSession();

        @NotNull final List<TaskDTO> tasks = serviceLocator.getTaskEndpoint().findAllTasks(session);
        int index = 1;
        for (@NotNull final TaskDTO task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
        System.out.println();
    }

}
