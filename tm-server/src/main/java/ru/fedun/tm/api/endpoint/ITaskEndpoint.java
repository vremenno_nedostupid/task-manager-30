package ru.fedun.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.dto.SessionDTO;
import ru.fedun.tm.dto.TaskDTO;

import java.util.Date;
import java.util.List;

public interface ITaskEndpoint {

    void createTask(
            @NotNull final SessionDTO sessionDTO,
            @NotNull final String taskName,
            @NotNull final String projectName,
            @NotNull final String description
    ) throws Exception;

    void updateTaskById(
            @NotNull final SessionDTO sessionDTO,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) throws Exception;

    void updateTaskStartDate(
            @NotNull final SessionDTO sessionDTO,
            @NotNull final String id,
            @NotNull final Date date
    ) throws Exception;

    void updateTaskCompleteDate(
            @NotNull final SessionDTO sessionDTO,
            @NotNull final String id,
            @NotNull final Date date
    ) throws Exception;

    @NotNull
    Long countAllTasks(@NotNull final SessionDTO sessionDTO) throws Exception;

    @NotNull
    Long countUserTasks(@NotNull final SessionDTO sessionDTO) throws Exception;

    @NotNull
    Long countProjectTasks(@NotNull final SessionDTO sessionDTO, @NotNull final String projectId) throws Exception;

    @NotNull
    Long countUserTasksOnProject(
            @NotNull final SessionDTO sessionDTO,
            @NotNull final String projectId
    ) throws Exception;

    @NotNull
    TaskDTO findTaskById(
            @NotNull final SessionDTO sessionDTO,
            @NotNull final String id
    ) throws Exception;

    @NotNull
    TaskDTO findTaskByName(@NotNull final SessionDTO sessionDTO, @NotNull final String name) throws Exception;

    @NotNull
    List<TaskDTO> findAllTasks(
            @NotNull final SessionDTO sessionDTO
    ) throws Exception;

    @NotNull
    List<TaskDTO> findAllTasksByUserId(@NotNull final SessionDTO sessionDTO) throws Exception;

    @NotNull
    public List<TaskDTO> findAllTasksByProjectId(
            @NotNull final SessionDTO sessionDTO,
            @NotNull final String projectId
    ) throws Exception;

    @NotNull
    public List<TaskDTO> findAllUserTasksOnProject(
            @NotNull final SessionDTO sessionDTO,
            @NotNull final String projectId
    ) throws Exception;

    public void removeTaskById(
            @NotNull final SessionDTO sessionDTO,
            @NotNull final String id
    ) throws Exception;

    public void removeTaskByName(
            @NotNull final SessionDTO sessionDTO,
            @NotNull final String name
    ) throws Exception;

    void clearTasks(@NotNull final SessionDTO sessionDTO) throws Exception;

    void clearTasksByUserId(@NotNull final SessionDTO sessionDTO) throws Exception;

    void clearTasksByProjectId(@NotNull final SessionDTO sessionDTO, @NotNull final String projectId) throws Exception;

    void clearUserTasksOnProject(
            @NotNull final SessionDTO sessionDTO,
            @NotNull final String projectId
    ) throws Exception;

}
