package ru.fedun.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.entity.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void persist(@NotNull E e);

    void merge(@NotNull E e);

    void remove(@NotNull E e);

}
