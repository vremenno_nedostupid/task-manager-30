package ru.fedun.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.dto.Domain;

public interface IDomainService {

    void load(@NotNull Domain domain);

    void export(@NotNull Domain domain);

}
