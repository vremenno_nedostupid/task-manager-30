package ru.fedun.tm.dto;

import lombok.Data;
import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.entity.Task;
import ru.fedun.tm.entity.User;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@XmlRootElement
public final class Domain implements Serializable {

    @NotNull
    private List<ProjectDTO> projects = new ArrayList<>();

    @NotNull
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    private List<User> users = new ArrayList<>();

}
