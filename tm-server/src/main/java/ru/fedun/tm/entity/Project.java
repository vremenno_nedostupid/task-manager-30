package ru.fedun.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "app_project")
public final class Project extends AbstractEntity {

    @NotNull
    @Column(columnDefinition = "TINYTEXT", nullable = false)
    private String name;

    @NotNull
    @Column(columnDefinition = "TEXT", nullable = false)
    private String description;

    @NotNull
    private Date startDate;

    @NotNull
    private Date completeDate;

    @NotNull
    @Column(updatable = false)
    private Date creationDate = new Date(System.currentTimeMillis());

    @NotNull
    @ManyToOne
    private User user;

    @NotNull
    @OneToMany(
            mappedBy = "project",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Task> tasks = new ArrayList<>();

}
