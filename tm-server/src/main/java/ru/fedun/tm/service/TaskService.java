package ru.fedun.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.api.repository.ITaskRepository;
import ru.fedun.tm.api.service.ITaskService;
import ru.fedun.tm.api.service.ServiceLocator;
import ru.fedun.tm.dto.TaskDTO;
import ru.fedun.tm.entity.Task;
import ru.fedun.tm.exception.empty.EmptyDescriptionException;
import ru.fedun.tm.exception.empty.EmptyIdException;
import ru.fedun.tm.exception.empty.EmptyTitleException;
import ru.fedun.tm.exception.empty.EmptyUserIdException;
import ru.fedun.tm.exception.incorrect.IncorrectCompleteDateException;
import ru.fedun.tm.exception.incorrect.IncorrectStartDateException;
import ru.fedun.tm.repository.TaskRepository;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    public TaskService(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public void persist(@NotNull Task task) {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.persist(task);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void merge(@NotNull final Task task) {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.merge(task);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@NotNull final Task task) {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.remove(task);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void createTask(
            @NotNull final String userId,
            @NotNull final String taskName,
            @NotNull final String projectName,
            @NotNull final String description
    ) throws Exception {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (taskName.isEmpty()) throw new EmptyTitleException();
        if (projectName.isEmpty()) throw new EmptyTitleException();
        if (description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Task task = new Task();
        task.setName(taskName);
        task.setDescription(description);
        task.setUser(serviceLocator.getUserService().findOneById(userId));
        task.setProject(serviceLocator.getProjectService().findOneByName(userId, projectName));
        persist(task);
    }

    @Override
    public void updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) throws Exception {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final Task task = findOneById(userId, id);
        if (!name.isEmpty()) task.setName(name);
        if (!description.isEmpty()) task.setDescription(description);
        if (name.isEmpty() && description.isEmpty()) return;
        merge(task);
    }

    @Override
    public void updateStartDate(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Date date
    ) throws Exception {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final Task task = findOneById(userId, id);
        if (date.before(new Date(System.currentTimeMillis()))) throw new IncorrectStartDateException();
        task.setStartDate(date);
        merge(task);
    }

    @Override
    public void updateCompleteDate(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Date date
    ) throws Exception {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final Task task = findOneById(userId, id);
        if (task.getStartDate().after(date)) throw new IncorrectCompleteDateException();
        task.setCompleteDate(date);
        merge(task);
    }

    @NotNull
    @Override
    public Long countAllTasks() {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final Long countOfTasks = repository.count();
        entityManager.close();
        return countOfTasks;
    }

    @NotNull
    @Override
    public Long countByUserId(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final Long countOfTasks = repository.countByUserId(userId);
        entityManager.close();
        return countOfTasks;
    }

    @NotNull
    @Override
    public Long countByProjectId(@NotNull final String projectId) throws Exception {
        if (projectId.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final Long countOfTasks = repository.countByProjectId(projectId);
        entityManager.close();
        return countOfTasks;
    }

    @NotNull
    @Override
    public Long countByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final Long countOfTasks = repository.countByUserIdAndProjectId(userId, projectId);
        entityManager.close();
        return countOfTasks;
    }

    @NotNull
    @Override
    public Task findOneById(@NotNull final String userId, @NotNull final String id) throws Exception {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final Task task = repository.findOneById(userId, id);
        entityManager.close();
        return task;
    }

    @NotNull
    @Override
    public Task findOneByName(@NotNull final String userId, @NotNull final String name) throws Exception {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyTitleException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final Task task = repository.findOneByName(userId, name);
        entityManager.close();
        return task;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll() {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final List<TaskDTO> tasksDTO = TaskDTO.toDTO(repository.findAll());
        entityManager.close();
        return tasksDTO;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByUserId(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final List<TaskDTO> tasksDTO = TaskDTO.toDTO(repository.findAllByUserId(userId));
        entityManager.close();
        return tasksDTO;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(@NotNull final String projectId) throws Exception {
        if (projectId.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final List<TaskDTO> tasksDTO = TaskDTO.toDTO(repository.findAllByProjectId(projectId));
        entityManager.close();
        return tasksDTO;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final List<TaskDTO> tasksDTO = TaskDTO.toDTO(repository.findAllByUserIdAndProjectId(userId, projectId));
        entityManager.close();
        return tasksDTO;
    }

    @Override
    public void removeOneById(@NotNull final String userId, @NotNull final String id) throws Exception {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeOneById(userId, id);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeOneByName(@NotNull final String userId, @NotNull final String name) throws Exception {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyTitleException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeOneByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAll() {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeAll();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeAllByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAllByProjectId(@NotNull final String projectId) throws Exception {
        if (projectId.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeAllByProjectId(projectId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAllByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeAllByUserIdAndProjectId(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

}
